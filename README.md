# Data buffer service

Data storage service.

## Functionality

- store data
- retrieve data in FIFO order
- supports data lifetime limit

## Deployment configuration

### Dependencies

- redis - database backend

### Accessibility

No-authentication public service.

### Initialization

No initialization needed.

### State[less/ful]

Stateful service. When redis volume deleted, all data will be lost.

### Replicability

Replicable without limits.

### Environments

No special environment needed.

### Secrets

Service requires no secrets.

### Configuration

- `PORT`- port for application server to listen
- `REDIS_URL` - redis url for service to connect to
- `DATA_LIFETIME_MS` - time limit in milliseconds between storing and retrieving the data entry. Data entries which exceed the limit will be filtered out while retrieving.

## Running

From repository root

```
docker-compose up --build
```

To add a data entry, send a POST request to `localhost:8080/give` with data as a body of request. E.g.,

```
curl -X POST localhost:8080/give -d "my data"
```

To retrieve a data entry, send a POST request to `localhost:8080/receive/{count}`. E.g.,

```
curl -X POST localhost:8080/receive/1
```

Note that if `count` requested exceeds count of data entries contained in database, response will be "OK" and only the available data entries will be returned.
