FROM node:12
WORKDIR /app
COPY *.json ./
RUN npm ci
COPY ./src .
CMD ["npm", "start"]