import * as Redis from "ioredis";
import { URL } from "url";
import { IFifoStringStorage } from "./IFifoStringStorage";
import { RedisKeys } from "./RedisKeys";

export class RedisClient implements IFifoStringStorage {
  private readonly redis: Redis.Redis;
  constructor(redisUrl: URL) {
    const url = redisUrl.toString();
    console.log(`RedisClient: connecting to ${url}`);
    this.redis = new Redis(url);
    console.log(`RedisClient: connected to ${url}`);
  }
  async give(data: string) {
    console.log(`RedisClient: giving ${data}`);
    await this.redis.rpush(RedisKeys.dataQueue(), data);
  }
  async recieve(count: number) {
    const maxCount = await this.redis.llen(RedisKeys.dataQueue());
    console.log(`RedisClient: elements left in database ${maxCount}`);
    console.log(`RedisClient: recieving ${count}`);
    const countToRequest = Math.min(count, maxCount);
    // "ioredis" does not support "count" parameter of "lpop" command,
    // so using "ioredis.pipeline" is seemingly the only option here.
    const pipeline = this.redis.pipeline();
    for (let i = 0; i < countToRequest; i += 1) {
      pipeline.lpop(RedisKeys.dataQueue());
    }
    const results = await pipeline.exec();
    // Extract responses ignoring error ones.
    return results.filter(([status, _]) => status === null).map(([_, response]) => response);
  }
}
