import { IFifoStringStorage, IFifoStringStorageController } from "./IFifoStringStorage";

export interface IDeserializedData {
  data: string;
  expirationTime: number;
}

export class FifoStringStorageController implements IFifoStringStorageController {
  private readonly storage: IFifoStringStorage;
  private readonly dataLifetimeMs: number;
  constructor(storage: IFifoStringStorage, dataLifetimeMs: number) {
    this.storage = storage;
    this.dataLifetimeMs = dataLifetimeMs;
  }
  async give(data: string): Promise<void> {
    console.log(`FifoStringStorageController: giving ${data}`);
    const deserializedData: IDeserializedData = { data, expirationTime: Date.now() + this.dataLifetimeMs };
    const serializedData = JSON.stringify(deserializedData);
    await this.storage.give(serializedData);
  }
  async recieve(count: number): Promise<string[]> {
    console.log(`FifoStringStorageController: recieving ${count}`);
    const currentTimeMs = Date.now();
    let countToRecieve = count;
    let result: string[] = [];
    let lastResponseLenght = 0;
    do {
      const relevantData = (await this.storage.recieve(countToRecieve))
        .map<IDeserializedData>(r => JSON.parse(r))
        .filter(r => currentTimeMs < r.expirationTime)
        .map(r => r.data);
      console.log(`FifoStringStorageController: extracted relevant elements count ${relevantData.length}`);
      result.push(...relevantData);
      lastResponseLenght = relevantData.length;
      countToRecieve -= relevantData.length;
    } while (countToRecieve !== 0 && lastResponseLenght !== 0);
    return result;
  }
}
