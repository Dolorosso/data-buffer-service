export interface IFifoStringStorage {
  give(data: string): Promise<void>;
  recieve(count: number): Promise<string[]>;
}

export interface IFifoStringStorageController extends IFifoStringStorage {}
