import * as bodyParser from "body-parser";
import * as express from "express";
import { URL } from "url";
import { FifoStringStorageController } from "./storage/FifoStringStorageController";
import { IFifoStringStorage } from "./storage/IFifoStringStorage";
import { RedisClient } from "./storage/RedisClient";

interface IAppConfiguration {
  readonly port: number;
  readonly redisUrl: URL;
  readonly dataLifetimeMs: number;
}

function configure(): IAppConfiguration {
  if (process.env.PORT === undefined) {
    throw new Error(`PORT environment variable is required but undefined`);
  }
  if (process.env.REDIS_URL === undefined) {
    throw new Error(`REDIS_URL environment variable is required but undefined`);
  }
  if (process.env.DATA_LIFETIME_MS === undefined) {
    throw new Error(`DATA_LIFETIME_MS environment variable is required but undefined`);
  }
  const configuration: IAppConfiguration = {
    port: parseInt(process.env.PORT),
    redisUrl: new URL(process.env.REDIS_URL),
    dataLifetimeMs: parseInt(process.env.DATA_LIFETIME_MS),
  };
  console.log(`Configuration loaded ${JSON.stringify(configuration)}`);
  return configuration;
}

console.log(`Starting app`);
const configuration = configure();

const storage: IFifoStringStorage = new FifoStringStorageController(new RedisClient(configuration.redisUrl), configuration.dataLifetimeMs);

const app = express();
app.use(bodyParser.text({ type: "*/*" }));
app.post("/give", async (req, res) => {
  const data = req.body;
  console.log(`Giving data ${data}`);
  await storage.give(data);
  res.end();
});
app.post("/receive/:count", async (req, res) => {
  const count = parseInt(req.params.count);
  console.log(`Recieving count ${count}`);
  const result = await storage.recieve(count);
  res.send(JSON.stringify(result));
});

console.log(`Listening on port ${configuration.port}`);
app.listen(configuration.port);
